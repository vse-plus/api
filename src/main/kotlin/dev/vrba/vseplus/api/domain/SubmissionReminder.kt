package dev.vrba.vseplus.api.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime

@Table("submission_reminders")
data class SubmissionReminder(
    @Id
    @Column("id")
    val id: Int = 0,

    @Column("account_id")
    val account: Int,

    @Column("submission_id")
    val submission: Int,

    @Column("submission_name")
    val name: String,

    @Column("submission_course")
    val course: String,

    @Column("submission_due")
    val due: LocalDateTime,

    @Column("reminder_date")
    val reminder: LocalDateTime
)