package dev.vrba.vseplus.api.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime

@Table("notifications")
data class Notification(
    @Id
    @Column("id")
    val id: Int,

    @Column("published_on")
    val published: LocalDateTime,

    @Column("notification_text")
    val notificationText: String,

    @Column("notification_link")
    val notificationLink: String? = null
)