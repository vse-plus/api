package dev.vrba.vseplus.api.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime

@Table("timetable_events")
data class TimetableEvent(
    @Id
    val id: Int = 0,

    @Column("account_id")
    val account: Int,

    @Column("entry_datetime")
    val datetime: LocalDateTime,

    @Column("entry_course")
    val course: String,

    @Column("event_type")
    val type: String,

    @Column("event_note")
    val note: String
)
