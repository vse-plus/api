package dev.vrba.vseplus.api.service

import dev.vrba.vseplus.api.domain.Account
import dev.vrba.vseplus.api.repository.AccountRepository
import dev.vrba.vseplus.api.security.jwt.JwtSupport
import org.springframework.stereotype.Service
import java.nio.ByteBuffer
import java.security.SecureRandom
import kotlin.random.Random

@Service
class AccountService(
    private val repository: AccountRepository,
    private val support: JwtSupport,
    private val emails: EmailService
) {

    suspend fun createAccount(username: String): Account {
        val account = repository.findByUsername(username) ?: Account(username = username)
        val code = createVerificationCode()

        emails.sendVerificationEmail(account.email, code)

        return repository.save(account.copy(code = code))
    }

    suspend fun verifyAccount(username: String, code: String): String? {
        val account = repository.findByUsername(username)

        if (account?.code != code) {
            return null
        }

        val updated = repository.save(account.copy(code = null))
        val token = support.generateToken(updated.username)

        return token.credentials
    }

    private fun createVerificationCode(): String {
        val seed = SecureRandom.getSeed(Long.SIZE_BYTES)
        val random = Random(ByteBuffer.wrap(seed).long)

        val charset = ('A' .. 'Z') + ('0' .. '9')

        return generateSequence { charset.random(random) }
            .take(8)
            .joinToString("")
    }

}