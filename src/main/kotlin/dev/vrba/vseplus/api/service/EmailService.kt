package dev.vrba.vseplus.api.service

import jakarta.mail.Message
import jakarta.mail.internet.InternetAddress
import jakarta.mail.internet.MimeBodyPart
import jakarta.mail.internet.MimeMultipart
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service
import org.thymeleaf.context.Context
import org.thymeleaf.spring6.SpringWebFluxTemplateEngine
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@Service
class EmailService(
    private val mailer: JavaMailSender,
    private val engine: SpringWebFluxTemplateEngine,
    private val formatter: DateTimeFormatter
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.qualifiedName)

    private val address = "vse-plus@vrba.dev"

    fun sendVerificationEmail(email: String, code: String) {
        logger.info("Sending verification code $code to $email")

        val subject = "Verifikační kód VŠE+"
        val text = "Verifikační kód VŠE+ pro účet $email je $code."
        val html = engine.process("emails/verification", Context().apply {
            setVariable("email", email)
            setVariable("code", code)
        })

        sendMultipartEmail(text, html, email, subject)
    }

    fun sendSubmissionReminderNotification(email: String, name: String, course: String, due: LocalDateTime) {
        logger.info("Sending submission reminder notification to $email")

        val formatted = due.format(formatter)
        val subject = "Připomenutí odevzdávárny $name"
        val text = "Připomenutí odevzdávárny $name z předmětu $course, která se uzavře $formatted"
        val html = engine.process("emails/submission-reminder", Context().apply {
            setVariable("name", name)
            setVariable("course", course)
            setVariable("due", formatted)
        })

        sendMultipartEmail(text, html, email, subject)
    }

    @Suppress("UsePropertyAccessSyntax")
    private fun sendMultipartEmail(text: String, html: String, recipient: String, subject: String) {
        val message = mailer.createMimeMessage()
        val multipart = MimeMultipart(
            "alternative",
            MimeBodyPart().apply { setContent(text, "text/plain; charset=UTF-8") },
            MimeBodyPart().apply { setContent(html, "text/html; charset=UTF-8") }
        )

        message.setFrom(InternetAddress(address, "VŠE+", "UTF-8"))
        message.setRecipient(Message.RecipientType.TO, InternetAddress(recipient))
        message.setSubject(subject, "UTF-8")
        message.setContent(multipart)

        mailer.send(message)
    }
}