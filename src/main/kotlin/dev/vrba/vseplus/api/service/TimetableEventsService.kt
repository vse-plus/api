package dev.vrba.vseplus.api.service

import dev.vrba.vseplus.api.domain.Account
import dev.vrba.vseplus.api.domain.TimetableEvent
import dev.vrba.vseplus.api.repository.TimetableEventsRepository
import kotlinx.coroutines.flow.toList
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class TimetableEventsService(private val repository: TimetableEventsRepository) {

    suspend fun listEventsForAccount(account: Account): List<TimetableEvent> {
        return repository.findAllByAccount(account.id).toList()
    }

    suspend fun createTimetableEvent(account: Account, course: String, datetime: LocalDateTime, type: String, note: String): List<TimetableEvent> {
        repository.save(
            TimetableEvent(
                account = account.id,
                datetime = datetime,
                course = course,
                type = type,
                note = note
            )
        )

        return listEventsForAccount(account)
    }

    suspend fun deleteTimetableEvent(account: Account, id: Int): List<TimetableEvent>? {
        val event = repository.findByIdAndAccount(id, account.id) ?: return null

        repository.delete(event)

        return listEventsForAccount(account)
    }

}
