package dev.vrba.vseplus.api.service

import dev.vrba.vseplus.api.domain.Account
import dev.vrba.vseplus.api.domain.SubmissionReminder
import dev.vrba.vseplus.api.repository.AccountRepository
import dev.vrba.vseplus.api.repository.SubmissionReminderRepository
import kotlinx.coroutines.flow.toList
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.Clock
import java.time.LocalDateTime

@Service
class SubmissionRemindersService(
    private val reminderRepository: SubmissionReminderRepository,
    private val accountRepository: AccountRepository,
    private val emailService: EmailService,
    private val clock: Clock
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.qualifiedName)

    suspend fun getSubmissionReminders(account: Account): List<SubmissionReminder> {
        return reminderRepository.findAllByAccount(account.id).toList()
    }

    suspend fun getSubmissionReminders(account: Account, submission: Int): List<SubmissionReminder> {
        return reminderRepository.findAllByAccountAndSubmission(account.id, submission).toList()
    }

    suspend fun createSubmissionReminder(
        account: Account,
        submission: Int,
        name: String,
        course: String,
        due: LocalDateTime,
        reminder: LocalDateTime
    ): List<SubmissionReminder> {
        // This indicates that the insis page was loaded from the browser cache
        if (due < LocalDateTime.now(clock)) {
            throw IllegalArgumentException("The due date cannot be in the past!")
        }

        if (due < reminder) {
            throw IllegalArgumentException("The reminder date cannot be before the due date!")
        }

        // Do not create duplicated reminder entries
        if (!reminderRepository.existsByAccountAndSubmissionAndDueAndReminder(account.id, submission, due, reminder)) {
            reminderRepository.save(
                SubmissionReminder(
                    account = account.id,
                    submission = submission,
                    name = name,
                    course = course,
                    due = due,
                    reminder = reminder
                )
            )
        }

        return reminderRepository.findAllByAccountAndSubmission(account.id, submission).toList()
    }

    suspend fun cancelSubmissionReminder(account: Account, submission: Int, reminder: Int): List<SubmissionReminder>? {
        val reminders = reminderRepository.findAllByAccountAndSubmission(account.id, submission).toList()
        val removed = reminders.firstOrNull { it.id == reminder } ?: return null

        reminderRepository.delete(removed)

        return reminders - removed
    }


    suspend fun sendPendingSubmissionRemindersNotifications() {
        logger.info("Sending out pending submission reminders notifications")

        val reminders = reminderRepository.findAllByReminderBefore(LocalDateTime.now(clock)).toList()
        val accounts = reminders.map { it.account }.toSet()
            .let { accountRepository.findAllById(it) }
            .toList()
            .associateBy { it.id }

        reminders.forEach {
            val account = accounts[it.account] ?: return@forEach
            emailService.sendSubmissionReminderNotification(account.email, it.name, it.course, it.due)
        }

        reminderRepository.deleteAll(reminders)
    }
}