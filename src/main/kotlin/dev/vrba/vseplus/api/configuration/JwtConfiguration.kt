package dev.vrba.vseplus.api.configuration

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "spring.jwt")
data class JwtConfiguration(
    val secret: String,
    val issuer: String
)