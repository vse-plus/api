package dev.vrba.vseplus.api.controller.api

import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.reactive.result.view.RedirectView

@Controller
@RequestMapping("/")
class HomeController {

    @GetMapping
    fun redirect(@RequestHeader(HttpHeaders.USER_AGENT) userAgent: String): RedirectView {
        val url = when {
            userAgent.contains("Firefox/") -> "https://addons.mozilla.org/en-US/firefox/addon/v%C5%A1e/"
            userAgent.contains("Chrome/") -> "https://chrome.google.com/webstore/detail/hbngcjlobkadngdbbaknlgmholembfbj"
            else -> "https://github.com/vse-plus"
        }

        return RedirectView(url)
    }

}