package dev.vrba.vseplus.api.controller.api

import dev.vrba.vseplus.api.domain.Account
import dev.vrba.vseplus.api.request.events.CreateTimetableEventRequest
import dev.vrba.vseplus.api.response.events.TimetableEventsResponse
import dev.vrba.vseplus.api.response.events.dto.toDto
import dev.vrba.vseplus.api.service.TimetableEventsService
import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/timetable-events")
class TimetableEventsController(private val service: TimetableEventsService) {

    @GetMapping
    suspend fun listTimetableEvents(@AuthenticationPrincipal account: Account): ResponseEntity<TimetableEventsResponse> {
        val events = service.listEventsForAccount(account)
        val response = TimetableEventsResponse(events.map { it.toDto() })

        return ResponseEntity.ok(response)
    }

    @PostMapping("/create")
    suspend fun createTimetableEvent(@AuthenticationPrincipal account: Account, @Valid @RequestBody request: CreateTimetableEventRequest): ResponseEntity<TimetableEventsResponse> {
        val events = service.createTimetableEvent(account, request.course, request.datetime, request.type, request.note)
        val response = TimetableEventsResponse(events.map { it.toDto() })

        return ResponseEntity.ok(response)
    }

    @PostMapping("/{id}/delete")
    suspend fun deleteTimetableEvent(@AuthenticationPrincipal account: Account, @PathVariable id: Int): ResponseEntity<TimetableEventsResponse> {
        val events = service.deleteTimetableEvent(account, id) ?: return ResponseEntity.notFound().build()
        val response = TimetableEventsResponse(events.map { it.toDto() })

        return ResponseEntity.ok(response)
    }

}