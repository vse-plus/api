package dev.vrba.vseplus.api.controller.api

import dev.vrba.vseplus.api.request.authentication.CreateAccountRequest
import dev.vrba.vseplus.api.request.authentication.VerifyAccountRequest
import dev.vrba.vseplus.api.response.authentication.CreateAccountResponse
import dev.vrba.vseplus.api.response.authentication.VerifyAccountResponse
import dev.vrba.vseplus.api.service.AccountService
import jakarta.validation.Valid
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/account")
class AccountController(private val service: AccountService) {

    @PostMapping("/create")
    suspend fun createAccount(@Valid @RequestBody request: CreateAccountRequest): ResponseEntity<CreateAccountResponse> {
        val account = service.createAccount(request.username)
        val response = CreateAccountResponse(
            username = account.username,
            message = "Na email ${account.email} byl zaslán ověřovací kód"
        )

        return ResponseEntity.ok(response)
    }

    @PostMapping("/verify")
    suspend fun verifyAccount(@Valid @RequestBody request: VerifyAccountRequest): ResponseEntity<VerifyAccountResponse> {
        val token = service.verifyAccount(request.username, request.code) ?: return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build()
        val response = VerifyAccountResponse(
            username = request.username,
            token = token
        )

        return ResponseEntity.ok(response)
    }
}