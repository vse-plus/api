package dev.vrba.vseplus.api.controller.api

import dev.vrba.vseplus.api.domain.Account
import dev.vrba.vseplus.api.request.reminders.CreateReminderRequest
import dev.vrba.vseplus.api.response.reminders.AggregateRemindersResponse
import dev.vrba.vseplus.api.response.reminders.DetailedRemindersResponse
import dev.vrba.vseplus.api.response.reminders.dto.toDto
import dev.vrba.vseplus.api.service.SubmissionRemindersService
import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/submission-reminders")
class SubmissionRemindersController(private val service: SubmissionRemindersService) {

    @GetMapping
    suspend fun index(@AuthenticationPrincipal account: Account): ResponseEntity<AggregateRemindersResponse> {
        val reminders = service.getSubmissionReminders(account)
        val response = AggregateRemindersResponse(reminders.map { it.submission }.toSet())

        return ResponseEntity.ok(response)
    }

    @GetMapping("/{id}")
    suspend fun details(@AuthenticationPrincipal account: Account, @PathVariable id: Int): ResponseEntity<DetailedRemindersResponse> {
        val reminders = service.getSubmissionReminders(account, id)
        val response = DetailedRemindersResponse(reminders.map { it.toDto() }.toSet())

        return ResponseEntity.ok(response)
    }

    @PostMapping("/create")
    suspend fun create(@AuthenticationPrincipal account: Account, @Valid @RequestBody request: CreateReminderRequest): ResponseEntity<DetailedRemindersResponse> {
        try {
            val reminders = service.createSubmissionReminder(
                account,
                request.submission,
                request.name,
                request.course,
                request.due,
                request.reminder
            )

            val response = DetailedRemindersResponse(reminders.map { it.toDto() }.toSet())

            return ResponseEntity.ok(response)
        }
        catch (exception: IllegalArgumentException) {
            return ResponseEntity
                .unprocessableEntity()
                .build()
        }
    }

    @PostMapping("/{submission}/{reminder}/cancel")
    suspend fun cancel(@AuthenticationPrincipal account: Account, @PathVariable submission: Int, @PathVariable reminder: Int): ResponseEntity<DetailedRemindersResponse> {
        val reminders = service.cancelSubmissionReminder(account, submission, reminder) ?: return ResponseEntity.notFound().build()

        val wrapped = reminders.map { it.toDto() }.toSet()
        val response = DetailedRemindersResponse(wrapped)

        return ResponseEntity.ok(response)
    }
}