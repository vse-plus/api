package dev.vrba.vseplus.api.controller.api

import dev.vrba.vseplus.api.repository.NotificationRepository
import dev.vrba.vseplus.api.response.notifications.NotificationsResponse
import dev.vrba.vseplus.api.response.notifications.dto.toDto
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.flow.toSet
import org.springframework.data.domain.Sort
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/notifications")
class NotificationsController(private val repository: NotificationRepository) {

    @GetMapping
    suspend fun index(): ResponseEntity<NotificationsResponse> {
        val notifications = repository.findAll(Sort.by(Sort.Order.desc("published")))
        val dto = notifications.toList().map { it.toDto() }
        val response = NotificationsResponse(dto.toSet())

        return ResponseEntity.ok(response)
    }

}