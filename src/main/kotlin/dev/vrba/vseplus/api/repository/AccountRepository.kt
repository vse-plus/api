package dev.vrba.vseplus.api.repository

import dev.vrba.vseplus.api.domain.Account
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface AccountRepository : CoroutineCrudRepository<Account, Int> {

    suspend fun findByUsername(username: String): Account?

}