package dev.vrba.vseplus.api.repository

import dev.vrba.vseplus.api.domain.Notification
import kotlinx.coroutines.flow.Flow
import org.springframework.data.domain.Sort
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface NotificationRepository : CoroutineCrudRepository<Notification, Int> {

    suspend fun findAll(sort: Sort): Flow<Notification>

}