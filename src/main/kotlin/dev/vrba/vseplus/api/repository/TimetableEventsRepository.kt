package dev.vrba.vseplus.api.repository

import dev.vrba.vseplus.api.domain.TimetableEvent
import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TimetableEventsRepository : CoroutineCrudRepository<TimetableEvent, Int> {

    suspend fun findAllByAccount(account: Int): Flow<TimetableEvent>

    suspend fun findByIdAndAccount(id: Int, account: Int): TimetableEvent?

}
