package dev.vrba.vseplus.api.repository

import dev.vrba.vseplus.api.domain.SubmissionReminder
import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface SubmissionReminderRepository : CoroutineCrudRepository<SubmissionReminder, Int> {

    suspend fun findAllByAccount(account: Int): Flow<SubmissionReminder>

    suspend fun findAllByAccountAndSubmission(account: Int, submission: Int): Flow<SubmissionReminder>

    suspend fun findAllByReminderBefore(now: LocalDateTime): Flow<SubmissionReminder>

    suspend fun existsByAccountAndSubmissionAndDueAndReminder(account: Int, submission: Int, due: LocalDateTime, reminder: LocalDateTime): Boolean

}