package dev.vrba.vseplus.api

import dev.vrba.vseplus.api.configuration.JwtConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.reactive.ReactiveUserDetailsServiceAutoConfiguration
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.reactive.config.EnableWebFlux
import java.time.Clock
import java.time.ZoneId
import java.time.format.DateTimeFormatter

@EnableWebFlux
@EnableScheduling
@EnableConfigurationProperties(JwtConfiguration::class)
@SpringBootApplication(exclude = [ReactiveUserDetailsServiceAutoConfiguration::class])
class VsePlusApiApplication {

    @Bean
    fun clock(): Clock = Clock.system(ZoneId.of("Europe/Prague"))

    @Bean
    fun formatter(): DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")

}

fun main(args: Array<String>) {
    runApplication<VsePlusApiApplication>(*args)
}
