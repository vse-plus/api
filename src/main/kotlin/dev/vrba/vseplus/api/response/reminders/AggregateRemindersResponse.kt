package dev.vrba.vseplus.api.response.reminders

class AggregateRemindersResponse(
    // List of reminder IDs that have a reminder associated
    val reminders: Set<Int>
)