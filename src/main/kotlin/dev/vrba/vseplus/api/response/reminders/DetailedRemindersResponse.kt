package dev.vrba.vseplus.api.response.reminders

import dev.vrba.vseplus.api.response.reminders.dto.SubmissionReminderDto

class DetailedRemindersResponse(
    val reminders: Set<SubmissionReminderDto>
)