package dev.vrba.vseplus.api.response.reminders.dto

import dev.vrba.vseplus.api.domain.SubmissionReminder
import java.time.format.DateTimeFormatter

data class SubmissionReminderDto(
    val id: Int,
    val submission: Int,
    val name: String,
    val course: String,
    val due: String,
    val reminder: String
)

fun SubmissionReminder.toDto(): SubmissionReminderDto = SubmissionReminderDto(
    this.id,
    this.submission,
    this.name,
    this.course,
    this.due.toString(),
    this.reminder.toString(),
)