package dev.vrba.vseplus.api.response.notifications

import dev.vrba.vseplus.api.response.notifications.dto.NotificationDto

data class NotificationsResponse(
    val notifications: Set<NotificationDto>
)