package dev.vrba.vseplus.api.response.authentication

data class CreateAccountResponse(
    val username: String,
    val message: String
)