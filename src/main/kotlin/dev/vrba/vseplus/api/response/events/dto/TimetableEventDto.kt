package dev.vrba.vseplus.api.response.events.dto

import dev.vrba.vseplus.api.domain.TimetableEvent

data class TimetableEventDto(
    val id: Int,
    val course: String,
    val datetime: String,
    val type: String,
    val note: String
)

fun TimetableEvent.toDto() = TimetableEventDto(id, course, datetime.toString(), type, note)