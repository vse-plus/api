package dev.vrba.vseplus.api.response.notifications.dto

import dev.vrba.vseplus.api.domain.Notification
import java.time.LocalDateTime

data class NotificationDto(
    val published: String,
    val text: String,
    val link: String?
)

fun Notification.toDto(): NotificationDto {
    return NotificationDto(
        this.published.toString(),
        this.notificationText,
        this.notificationLink
    )
}