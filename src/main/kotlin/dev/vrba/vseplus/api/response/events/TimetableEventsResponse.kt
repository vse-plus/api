package dev.vrba.vseplus.api.response.events

import dev.vrba.vseplus.api.response.events.dto.TimetableEventDto

data class TimetableEventsResponse(
    val events: List<TimetableEventDto>
)