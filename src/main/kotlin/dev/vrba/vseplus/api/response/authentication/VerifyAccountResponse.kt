package dev.vrba.vseplus.api.response.authentication

class VerifyAccountResponse(
    val username: String,
    val token: String
)