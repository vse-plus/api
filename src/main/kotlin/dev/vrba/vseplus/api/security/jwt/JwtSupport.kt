package dev.vrba.vseplus.api.security.jwt

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import dev.vrba.vseplus.api.configuration.JwtConfiguration
import dev.vrba.vseplus.api.domain.Account
import org.springframework.core.env.Environment
import org.springframework.core.env.Profiles
import org.springframework.stereotype.Component
import java.time.Clock
import java.time.Duration
import java.time.Instant

@Component
class JwtSupport(
    private val clock: Clock,
    private val configuration: JwtConfiguration,
    private val environment: Environment
) {
    private val algorithm: Algorithm = Algorithm.HMAC512(configuration.secret)

    private val verifier: JWTVerifier = JWT.require(algorithm).withIssuer(configuration.issuer).build()

    init {
        val development = environment.acceptsProfiles(Profiles.of("development", "test"))

        if (!development && configuration.secret.length < 64) {
            throw IllegalStateException("The configured JWT secret has to be at least 64 bytes long.")
        }
    }

    fun generateToken(username: String): JwtBearerToken {
        return JwtBearerToken(
            JWT.create()
                .withIssuer(configuration.issuer)
                .withIssuedAt(Instant.now(clock))
                .withExpiresAt(Instant.now(clock) + Duration.ofDays(180))
                .withSubject(username)
                .sign(algorithm)
        )
    }

    fun validateToken(token: JwtBearerToken, account: Account): Boolean {
        val parsed = verifier.verify(token.principal)

        val expired = parsed.expiresAtAsInstant.isBefore(Instant.now(clock))
        val username = retrieveUsername(token)

        return !expired && account.username == username
    }

    fun retrieveUsername(token: JwtBearerToken): String {
        return verifier.verify(token.credentials).subject
    }
}