package dev.vrba.vseplus.api.security

import dev.vrba.vseplus.api.security.jwt.JwtAuthenticationConverter
import dev.vrba.vseplus.api.security.jwt.JwtAuthenticationManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.config.web.server.invoke
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.ServerAuthenticationEntryPoint
import org.springframework.security.web.server.authentication.AuthenticationWebFilter
import org.springframework.security.web.server.util.matcher.PathPatternParserServerWebExchangeMatcher
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.util.AntPathMatcher
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.reactive.CorsWebFilter
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource
import org.springframework.web.reactive.config.CorsRegistry
import org.springframework.web.reactive.config.WebFluxConfigurer
import reactor.core.publisher.Mono

@Configuration
class SecurityConfiguration {

    @Bean
    fun securityWebFilterChain(
        http: ServerHttpSecurity,
        manager: JwtAuthenticationManager,
        converter: JwtAuthenticationConverter
    ): SecurityWebFilterChain {
        // Create a new authentication web filter for JWT, that can be registered to the filter chain
        val filter = AuthenticationWebFilter(manager).apply {
            setServerAuthenticationConverter(converter)
        }

        return http {
            authorizeExchange {
                authorize("/api/v1/account/*", permitAll)
                authorize("/api/v1/notifications", permitAll)
                authorize(PathPatternParserServerWebExchangeMatcher("/api/v1/**", HttpMethod.GET), hasRole("USER"))
                authorize(PathPatternParserServerWebExchangeMatcher("/api/v1/**", HttpMethod.POST), hasRole("USER"))
                authorize(anyExchange, permitAll)
            }

            exceptionHandling {
                authenticationEntryPoint = ServerAuthenticationEntryPoint { exchange, _ ->
                    Mono.fromRunnable {
                        exchange.response.statusCode = HttpStatus.UNAUTHORIZED
                        exchange.response.headers.set(HttpHeaders.WWW_AUTHENTICATE, "Bearer")
                    }
                }
            }

            addFilterAt(filter, SecurityWebFiltersOrder.AUTHENTICATION)

            csrf { disable() }
            httpBasic { disable() }
            formLogin { disable() }
            cors {}
        }
    }

    @Bean
    fun corsFilter(): CorsWebFilter {
        val source = UrlBasedCorsConfigurationSource()
        val config = CorsConfiguration().apply {
            allowCredentials = false
            allowedHeaders = listOf("*")
            allowedMethods = listOf("*")
            allowedOrigins = listOf("*")
        }

        source.registerCorsConfiguration("/api/v1/**", config)

        return CorsWebFilter(source)
    }
}