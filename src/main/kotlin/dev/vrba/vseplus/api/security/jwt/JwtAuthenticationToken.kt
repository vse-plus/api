package dev.vrba.vseplus.api.security.jwt

import dev.vrba.vseplus.api.domain.Account
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.authority.AuthorityUtils.createAuthorityList

class JwtAuthenticationToken(private val account: Account, private val token: String) : AbstractAuthenticationToken(createAuthorityList("ROLE_USER")) {
    override fun getPrincipal(): Account = account
    override fun getCredentials(): String = token
    override fun isAuthenticated(): Boolean = true
}