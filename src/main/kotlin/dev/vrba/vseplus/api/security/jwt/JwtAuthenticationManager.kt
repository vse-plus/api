package dev.vrba.vseplus.api.security.jwt

import dev.vrba.vseplus.api.repository.AccountRepository
import kotlinx.coroutines.reactor.mono
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
class JwtAuthenticationManager(private val support: JwtSupport, private val repository: AccountRepository) : ReactiveAuthenticationManager {

    override fun authenticate(authentication: Authentication?): Mono<Authentication> {
        if (authentication !is JwtBearerToken) {
            return Mono.empty()
        }

        return mono { validate(authentication) }.onErrorMap {
            if (it is AuthenticationException) it
            else InvalidJwtTokenException
        }
    }

    private suspend fun validate(token: JwtBearerToken): Authentication {
        val username = support.retrieveUsername(token)
        val principal = repository.findByUsername(username) ?: throw UsernameNotFoundException("Username did not match any record")

        if (!support.validateToken(token, principal)) {
            throw BadCredentialsException("Invalid or expired JWT token provided")
        }

        return JwtAuthenticationToken(principal, token.credentials)
    }
}