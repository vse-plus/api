package dev.vrba.vseplus.api.security.jwt

import org.springframework.security.core.AuthenticationException

object InvalidJwtTokenException : AuthenticationException("Invalid JWT token provided")