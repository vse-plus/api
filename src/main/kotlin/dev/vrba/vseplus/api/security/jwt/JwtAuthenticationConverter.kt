package dev.vrba.vseplus.api.security.jwt

import org.springframework.http.HttpHeaders
import org.springframework.security.core.Authentication
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

@Component
class JwtAuthenticationConverter : ServerAuthenticationConverter {

    override fun convert(exchange: ServerWebExchange?): Mono<Authentication> {
        return Mono.justOrEmpty(exchange)
            .mapNotNull { it.request.headers.getFirst(HttpHeaders.AUTHORIZATION) ?: "" }
            .filter { it.startsWith("Bearer ") }
            .map { it.removePrefix("Bearer ") }
            .map { JwtBearerToken(it.trim()) }
    }

}