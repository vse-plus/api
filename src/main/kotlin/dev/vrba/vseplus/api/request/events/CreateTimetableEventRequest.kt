package dev.vrba.vseplus.api.request.events

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Size
import java.time.LocalDateTime

data class CreateTimetableEventRequest(
    @field:NotBlank
    @field:Size(max = 128)
    val course: String,

    @field:Size(max = 128)
    val type: String,

    @field:Size(max = 1024)
    val note: String,

    val datetime: LocalDateTime
)