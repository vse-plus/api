package dev.vrba.vseplus.api.request.authentication

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Pattern
import jakarta.validation.constraints.Size

class VerifyAccountRequest(
    @field:NotBlank
    @field:Size(min = 6, max = 6)
    @field:Pattern(regexp = "^[a-z]+[a-z0-9]+$")
    val username: String,

    @field:NotBlank
    @field:Size(min = 8, max = 8)
    @field:Pattern(regexp = "^[A-Z0-9]+$")
    val code: String
)
