package dev.vrba.vseplus.api.request.authentication

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Pattern
import jakarta.validation.constraints.Size

class CreateAccountRequest(
    @field:NotBlank
    @field:Size(min = 6, max = 6)
    @field:Pattern(regexp = "^[a-z]+[a-z0-9]+$")
    val username: String
)