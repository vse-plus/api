package dev.vrba.vseplus.api.request.reminders

import jakarta.validation.constraints.Min
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Size
import java.time.LocalDateTime

class CreateReminderRequest(
    @field:Min(1)
    val submission: Int,

    @field:NotBlank
    @field:Size(min = 1, max = 128)
    val name: String,

    @field:NotBlank
    @field:Size(min = 1, max = 128)
    val course: String,

    // TODO: Add date time formatting

    val due: LocalDateTime,

    val reminder: LocalDateTime
)