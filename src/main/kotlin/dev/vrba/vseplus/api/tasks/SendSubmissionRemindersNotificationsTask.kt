package dev.vrba.vseplus.api.tasks

import dev.vrba.vseplus.api.service.EmailService
import dev.vrba.vseplus.api.service.SubmissionRemindersService
import kotlinx.coroutines.runBlocking
import org.springframework.core.env.Environment
import org.springframework.core.env.Profiles
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
class SendSubmissionRemindersNotificationsTask(
    private val environment: Environment,
    private val service: SubmissionRemindersService
) {
    @Scheduled(fixedRate = 5, timeUnit = TimeUnit.MINUTES)
    fun run(): Unit = runBlocking {
        // Do not run this task during tests
        if (!environment.acceptsProfiles(Profiles.of("test"))) {
            service.sendPendingSubmissionRemindersNotifications()
        }
    }
}
