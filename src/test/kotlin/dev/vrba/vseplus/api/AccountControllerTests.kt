package dev.vrba.vseplus.api

import com.ninjasquad.springmockk.MockkBean
import dev.vrba.vseplus.api.domain.Account
import dev.vrba.vseplus.api.repository.AccountRepository
import dev.vrba.vseplus.api.service.EmailService
import io.mockk.coEvery
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureWebTestClient
class AccountControllerTests {

    @Autowired
    private lateinit var client: WebTestClient

    @MockkBean
    private lateinit var repository: AccountRepository

    @MockkBean
    private lateinit var emailService: EmailService

    @Test
    fun `test creating accounts with invalid username`() {
        client.get()
            .uri("/api/v1/account/create")
            .exchange()
            .expectStatus()
            .is4xxClientError

        client.post()
            .uri("/api/v1/account/create")
            .exchange()
            .expectStatus()
            .isBadRequest

        client.post()
            .uri("/api/v1/account/create")
            .bodyValue(mapOf("username" to "invalid"))
            .exchange()
            .expectStatus()
            .isBadRequest

        client.post()
            .uri("/api/v1/account/create")
            .bodyValue(mapOf("username" to 0))
            .exchange()
            .expectStatus()
            .isBadRequest

        client.post()
            .uri("/api/v1/account/create")
            .bodyValue(mapOf("username" to "04vrbj"))
            .exchange()
            .expectStatus()
            .isBadRequest
    }

    @Test
    fun `test creating accounts with valid username`() {
        val slot = slot<Account>()

        coEvery { repository.findByUsername("vrbj04") } returns null
        coEvery { repository.save(capture(slot)) } returnsArgument 0

        every { emailService.sendVerificationEmail("vrbj04@vse.cz", any()) } returns Unit

        client.post()
            .uri("/api/v1/account/create")
            .bodyValue(mapOf("username" to "vrbj04"))
            .exchange()
            .expectStatus()
            .isOk
            .expectBody()
            .jsonPath("message").isEqualTo( "Na email vrbj04@vse.cz byl zaslán ověřovací kód")
            .jsonPath("username").isEqualTo( "vrbj04")

        assertTrue(slot.isCaptured)
        assertEquals("vrbj04", slot.captured.username)
        assertNotNull(slot.captured.code)

        verify { emailService.sendVerificationEmail("vrbj04@vse.cz", slot.captured.code!!) }
    }

    @Test
    fun `test verifying accounts without code`() {
        client.get()
            .uri("/api/v1/account/verify")
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.METHOD_NOT_ALLOWED)

        client.post()
            .uri("/api/v1/account/verify")
            .exchange()
            .expectStatus()
            .isBadRequest

        client.post()
            .uri("/api/v1/account/verify")
            .bodyValue(mapOf("username" to "vrbj04"))
            .exchange()
            .expectStatus()
            .isBadRequest
    }

    @Test
    fun `test verifying non-existing accounts`() {
        coEvery { repository.findByUsername("vrbj04") } returns null

        client.post()
            .uri("/api/v1/account/verify")
            .bodyValue(mapOf(
                "username" to "vrbj04",
                "code" to "A4EU4AX1"
            ))
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
    }

    @Test
    fun `test verifying accounts with an invalid code`() {
        val account = Account(id = 100, username = "vrbj04", code = "ABCDEFGH")

        coEvery { repository.findByUsername("vrbj04") } returns account

        client.post()
            .uri("/api/v1/account/verify")
            .bodyValue(mapOf(
                "username" to "vrbj04",
                "code" to "A4EU4AX1"
            ))
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
    }

    @Test
    fun `test verifying accounts with a valid code`() {
        val account = Account(id = 100, username = "vrbj04", code = "ABCDEFGH")
        val slot = slot<Account>()

        coEvery { repository.findByUsername("vrbj04") } returns account
        coEvery { repository.save(capture(slot)) } returnsArgument 0

        client.post()
            .uri("/api/v1/account/verify")
            .bodyValue(mapOf(
                "username" to "vrbj04",
                "code" to "ABCDEFGH"
            ))
            .exchange()
            .expectStatus()
            .isOk
            .expectBody()
            .jsonPath("token")
            .isNotEmpty
    }
}