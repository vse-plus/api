package dev.vrba.vseplus.api

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureWebTestClient
class HomeControllerTests {

    @Autowired
    private lateinit var client: WebTestClient

    @Test
    fun `test that homepage redirects firefox browsers to Mozilla addons`() {
        // https://www.whatismybrowser.com/guides/the-latest-user-agent/firefox
        val userAgents = setOf(
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:105.0) Gecko/20100101 Firefox/105.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 12.6; rv:105.0) Gecko/20100101 Firefox/105.0",
            "Mozilla/5.0 (X11; Linux i686; rv:105.0) Gecko/20100101 Firefox/105.0",
            "Mozilla/5.0 (Android 13; Mobile; rv:68.0) Gecko/68.0 Firefox/105.0",
            "Mozilla/5.0 (X11; Linux i686; rv:102.0) Gecko/20100101 Firefox/102.0"
        )

        userAgents.forEach {
            client.get()
                .uri("/")
                .header(HttpHeaders.USER_AGENT, it)
                .exchange()
                .expectStatus()
                .isSeeOther
                .expectHeader()
                .location("https://addons.mozilla.org/en-US/firefox/addon/v%C5%A1e/")
        }
    }

    @Test
    fun `test that homepage redirects chromium browsers to Google webstore`() {
        val userAgents = setOf(
            // https://www.whatismybrowser.com/guides/the-latest-user-agent/chrome
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36",
            "Mozilla/5.0 (Linux; Android 10; LM-Q720) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.5249.79 Mobile Safari/537.36",

            // https://www.whatismybrowser.com/guides/the-latest-user-agent/edge,
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 Edg/106.0.1370.34",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 Edg/106.0.1370.34",
            "Mozilla/5.0 (Linux; Android 10; HD1913) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.5249.79 Mobile Safari/537.36 EdgA/100.0.1185.50",

            // https://www.whatismybrowser.com/guides/the-latest-user-agent/opera
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 OPR/91.0.4516.20",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 OPR/91.0.4516.20",
            "Mozilla/5.0 (Linux; Android 10; VOG-L29) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.5249.79 Mobile Safari/537.36 OPR/63.3.3216.58675",

            // https://developers.whatismybrowser.com/useragents/parse/234792113-brave-linux-webkit
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Brave Chrome/84.0.4147.105 Safari/537.36"
        )

        userAgents.forEach {
            client.get()
                .uri("/")
                .header(HttpHeaders.USER_AGENT, it)
                .exchange()
                .expectStatus()
                .isSeeOther
                .expectHeader()
                .location("https://chrome.google.com/webstore/detail/hbngcjlobkadngdbbaknlgmholembfbj")
        }
    }

    @Test
    fun `test that homepage redirects unsupported browsers to github page`() {
        val userAgents = setOf(
            // https://www.whatismybrowser.com/guides/the-latest-user-agent/safari
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 12_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.0 Safari/605.1.15",
            "Mozilla/5.0 (iPhone; CPU iPhone OS 16_0_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.0 Mobile/15E148 Safari/604.1",

            // https://www.whatismybrowser.com/guides/the-latest-user-agent/internet-explorer
            "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)",
            "Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko"
        )

        userAgents.forEach {
            client.get()
                .uri("/")
                .header(HttpHeaders.USER_AGENT, it)
                .exchange()
                .expectStatus()
                .isSeeOther
                .expectHeader()
                .location("https://github.com/vse-plus")
        }
    }
}
