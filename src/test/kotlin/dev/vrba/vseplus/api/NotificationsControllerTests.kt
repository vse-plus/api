package dev.vrba.vseplus.api

import com.ninjasquad.springmockk.MockkBean
import dev.vrba.vseplus.api.domain.Notification
import dev.vrba.vseplus.api.repository.NotificationRepository
import io.mockk.coEvery
import kotlinx.coroutines.flow.flowOf
import org.hamcrest.core.IsNull.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient
import java.time.LocalDateTime
import java.time.Month


@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureWebTestClient
class NotificationsControllerTests {

    @Autowired
    private lateinit var client: WebTestClient

    @MockkBean
    private lateinit var notificationRepository: NotificationRepository

    @Test
    fun `test notifications can be fetched`() {
        coEvery { notificationRepository.findAll(any()) } coAnswers {
            flowOf(
                Notification(1, LocalDateTime.of(2022, Month.NOVEMBER, 10, 19, 30, 21), "Test notification"),
                Notification(2, LocalDateTime.of(2023, Month.FEBRUARY, 13, 10, 12, 37), "Check this cool cat video lmao", "https://cdn.discordapp.com/attachments/472407264700006420/1061346350098284554/cta.mp4"),
                Notification(3, LocalDateTime.of(2023, Month.FEBRUARY, 15, 21, 25, 41), "Another test notification", "https://cdn.discordapp.com/attachments/920296547210625024/981250162154430473/statistika.mp4")
            )
        }

        client.get()
            .uri("/api/v1/notifications")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.notifications").isArray
            .jsonPath("$.notifications.length()").isEqualTo(3)
            .jsonPath("$.notifications[0].published").isEqualTo("2022-11-10T19:30:21")
            .jsonPath("$.notifications[0].text").isEqualTo("Test notification")
            .jsonPath("$.notifications[0].link").value(nullValue())
            .jsonPath("$.notifications[1].published").isEqualTo("2023-02-13T10:12:37")
            .jsonPath("$.notifications[1].text").isEqualTo("Check this cool cat video lmao")
            .jsonPath("$.notifications[1].link").isEqualTo("https://cdn.discordapp.com/attachments/472407264700006420/1061346350098284554/cta.mp4")
            .jsonPath("$.notifications[2].published").isEqualTo("2023-02-15T21:25:41")
            .jsonPath("$.notifications[2].text").isEqualTo("Another test notification")
            .jsonPath("$.notifications[2].link").isEqualTo("https://cdn.discordapp.com/attachments/920296547210625024/981250162154430473/statistika.mp4")
    }
}