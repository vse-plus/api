package dev.vrba.vseplus.api

import com.ninjasquad.springmockk.MockkBean
import dev.vrba.vseplus.api.domain.Account
import dev.vrba.vseplus.api.domain.SubmissionReminder
import dev.vrba.vseplus.api.repository.AccountRepository
import dev.vrba.vseplus.api.repository.SubmissionReminderRepository
import dev.vrba.vseplus.api.security.jwt.JwtAuthenticationToken
import io.mockk.*
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.emptyFlow
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient
import java.time.*


@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureWebTestClient
class SubmissionRemindersTests {

    @Autowired
    private lateinit var client: WebTestClient

    @MockkBean
    private lateinit var accountRepository: AccountRepository

    @MockkBean
    private lateinit var reminderRepository: SubmissionReminderRepository

    @MockkBean
    private lateinit var clock: Clock

    @Test
    fun `test that user cannot access submission reminders without authentication`() {
        client.get()
            .uri("/api/v1/submission-reminders")
            .exchange()
            .expectStatus()
            .isUnauthorized

        client.get()
            .uri("/api/v1/submission-reminders/1")
            .exchange()
            .expectStatus()
            .isUnauthorized
    }

    @Test
    fun `test that user can load aggregated reminders`() {
        val account = Account(420, "vrbj04")
        val reminders = listOf(
            SubmissionReminder(
                id = 1, account = 420, 121430, "DÚ 2", "Účetnictví I. (1FU201)",
                LocalDateTime.of(2022, Month.OCTOBER, 4, 12, 45, 0),
                LocalDateTime.of(2022, Month.OCTOBER, 3, 12, 0, 0)
            ),
            SubmissionReminder(
                id = 2, account = 420, 121431, "DÚ 3", "Účetnictví I. (1FU201)",
                LocalDateTime.of(2022, Month.OCTOBER, 4, 12, 45, 0),
                LocalDateTime.of(2022, Month.OCTOBER, 3, 12, 0, 0)
            ),
            SubmissionReminder(
                id = 3, account = 420, 121431, "DÚ 3", "Účetnictví I. (1FU201)",
                LocalDateTime.of(2022, Month.OCTOBER, 4, 12, 45, 0),
                LocalDateTime.of(2022, Month.OCTOBER, 3, 18, 0, 0)
            )
        )

        val authentication = authenticate(account)

        coEvery { accountRepository.findByUsername("vrbj04") } returns account
        coEvery { reminderRepository.findAllByAccount(account.id) } returns reminders.asFlow()

        client
            .mutateWith(mockAuthentication(authentication))
            .get()
            .uri("/api/v1/submission-reminders")
            .exchange()
            .expectStatus()
            .isOk
            .expectBody()
            .jsonPath("reminders")
            .isArray
            .jsonPath("reminders")
            .isEqualTo(arrayListOf(121430, 121431))

        coVerify { reminderRepository.findAllByAccount(account.id) }
    }

    @Test
    fun `test that user can load detailed reminders`() {
        val account = Account(420, "vrbj04")
        val reminders = listOf(
            SubmissionReminder(
                id = 2, account = 420, 121431, "DÚ 3", "Účetnictví I. (1FU201)",
                LocalDateTime.of(2022, Month.OCTOBER, 4, 12, 45, 0),
                LocalDateTime.of(2022, Month.OCTOBER, 3, 12, 0, 0)
            ),
            SubmissionReminder(
                id = 3, account = 420, 121431, "DÚ 3", "Účetnictví I. (1FU201)",
                LocalDateTime.of(2022, Month.OCTOBER, 4, 12, 45, 0),
                LocalDateTime.of(2022, Month.OCTOBER, 3, 18, 0, 0)
            )
        )

        val authentication = authenticate(account)

        coEvery { accountRepository.findByUsername("vrbj04") } returns account
        coEvery { reminderRepository.findAllByAccountAndSubmission(account.id, 121431) } returns reminders.asFlow()

        client
            .mutateWith(mockAuthentication(authentication))
            .get()
            .uri("/api/v1/submission-reminders/121431")
            .exchange()
            .expectStatus()
            .isOk
            .expectBody()
            .jsonPath("reminders")
            .isArray
            .jsonPath("reminders[0].id").isEqualTo(2)
            .jsonPath("reminders[0].submission").isEqualTo(121431)
            .jsonPath("reminders[0].name").isEqualTo("DÚ 3")
            .jsonPath("reminders[0].course").isEqualTo("Účetnictví I. (1FU201)")
            .jsonPath("reminders[0].due").isEqualTo("2022-10-04T12:45")
            .jsonPath("reminders[0].reminder").isEqualTo("2022-10-03T12:00")
            .jsonPath("reminders[1].id").isEqualTo(3)
            .jsonPath("reminders[1].submission").isEqualTo(121431)
            .jsonPath("reminders[1].name").isEqualTo("DÚ 3")
            .jsonPath("reminders[1].course").isEqualTo("Účetnictví I. (1FU201)")
            .jsonPath("reminders[1].due").isEqualTo("2022-10-04T12:45")
            .jsonPath("reminders[1].reminder").isEqualTo("2022-10-03T18:00")

        coVerify { reminderRepository.findAllByAccountAndSubmission(account.id, 121431) }
    }

    @Test
    fun `test that user cannot create reminders with due date in the past`() {
        val account = Account(420, "vrbj04")
        val authentication = authenticate(account)

        coEvery { accountRepository.findByUsername("vrbj04") } returns account

        // 🎄
        val now = ZonedDateTime.of(LocalDateTime.of(2022, Month.DECEMBER, 24, 23, 59, 10), ZoneId.of("Europe/Prague"))

        every { clock.zone } returns now.zone
        every { clock.instant() } returns now.toInstant()

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/submission-reminders/create")
            .bodyValue(
                mapOf(
                    "submission" to 121430,
                    "name" to "DÚ 2",
                    "course" to "Účetnictví I. (1FU201)",
                    "reminder" to "2022-10-03T12:00",
                    "due" to "2022-10-04T12:45",
                )
            )
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
    }

    @Test
    fun `test that user cannot create reminders with reminder date after the due date`() {
        val account = Account(420, "vrbj04")
        val authentication = authenticate(account)

        coEvery { accountRepository.findByUsername("vrbj04") } returns account

        val now = ZonedDateTime.of(LocalDateTime.of(2022, Month.SEPTEMBER, 30, 23, 59, 10), ZoneId.of("Europe/Prague"))

        every { clock.zone } returns now.zone
        every { clock.instant() } returns now.toInstant()

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/submission-reminders/create")
            .bodyValue(
                mapOf(
                    "submission" to 121430,
                    "name" to "DÚ 2",
                    "course" to "Účetnictví I. (1FU201)",
                    "reminder" to "2022-10-04T13:00",
                    "due" to "2022-10-04T12:45",
                )
            )
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
    }

    @Test
    fun `test that user can create valid reminders`() {
        val account = Account(420, "vrbj04")
        val slot = slot<SubmissionReminder>()
        val authentication = authenticate(account)
        val now = ZonedDateTime.of(LocalDateTime.of(2022, Month.SEPTEMBER, 30, 23, 59, 10), ZoneId.of("Europe/Prague"))

        every { clock.zone } returns now.zone
        every { clock.instant() } returns now.toInstant()
        coEvery { reminderRepository.save(capture(slot)) } returnsArgument 0
        coEvery { reminderRepository.findAllByAccountAndSubmission(account.id, 121430) } returns emptyFlow()
        coEvery { reminderRepository.existsByAccountAndSubmissionAndDueAndReminder(420, 121430, LocalDateTime.of(2022, Month.OCTOBER, 4, 12, 45, 0), LocalDateTime.of(2022, Month.OCTOBER, 3, 12, 0, 0) )} returns false

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/submission-reminders/create")
            .bodyValue(
                mapOf(
                    "submission" to 121430,
                    "name" to "DÚ 2",
                    "course" to "Účetnictví I. (1FU201)",
                    "reminder" to "2022-10-03T12:00:00",
                    "due" to "2022-10-04T12:45:00",
                )
            )
            .exchange()
            .expectStatus()
            .isOk
            .expectBody()
            .jsonPath("reminders")
            .isArray

        assertTrue(slot.isCaptured)
        assertEquals(121430, slot.captured.submission)
        assertEquals("DÚ 2", slot.captured.name)
        assertEquals("Účetnictví I. (1FU201)", slot.captured.course)
        assertEquals(LocalDateTime.of(2022, Month.OCTOBER, 3, 12, 0, 0), slot.captured.reminder)
        assertEquals(LocalDateTime.of(2022, Month.OCTOBER, 4, 12, 45, 0), slot.captured.due)
    }

    @Test
    fun `test that users can create entries with different reminder dates`() {
        val account = Account(420, "vrbj04")
        val slot = slot<SubmissionReminder>()
        val authentication = authenticate(account)
        val now = ZonedDateTime.of(LocalDateTime.of(2022, Month.SEPTEMBER, 30, 23, 59, 10), ZoneId.of("Europe/Prague"))

        every { clock.zone } returns now.zone
        every { clock.instant() } returns now.toInstant()
        coEvery { reminderRepository.save(capture(slot)) } returnsArgument 0
        coEvery { reminderRepository.findAllByAccountAndSubmission(account.id, 121430) } returns emptyFlow()
        coEvery { reminderRepository.existsByAccountAndSubmissionAndDueAndReminder(420, 121430,
            LocalDateTime.of(2022, Month.OCTOBER, 4, 12, 45, 0),
            LocalDateTime.of(2022, Month.OCTOBER, 3, 12, 0, 0),
        )} returns true

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/submission-reminders/create")
            .bodyValue(
                mapOf(
                    "submission" to 121430,
                    "name" to "DÚ 2",
                    "course" to "Účetnictví I. (1FU201)",
                    "reminder" to "2022-10-03T12:00:00",
                    "due" to "2022-10-04T12:45:00",
                )
            )
            .exchange()
            .expectStatus()
            .isOk
            .expectBody()
            .jsonPath("reminders")
            .isArray

        coVerify(exactly = 0) { reminderRepository.save(any()) }
        assertFalse(slot.isCaptured)
    }

    @Test
    fun `test that users cannot create duplicated entries`() {
        val account = Account(420, "vrbj04")
        val slot = slot<SubmissionReminder>()
        val authentication = authenticate(account)
        val now = ZonedDateTime.of(LocalDateTime.of(2022, Month.SEPTEMBER, 30, 23, 59, 10), ZoneId.of("Europe/Prague"))

        every { clock.zone } returns now.zone
        every { clock.instant() } returns now.toInstant()
        coEvery { reminderRepository.save(capture(slot)) } returnsArgument 0
        coEvery { reminderRepository.findAllByAccountAndSubmission(account.id, 121430) } returns emptyFlow()
        coEvery { reminderRepository.existsByAccountAndSubmissionAndDueAndReminder(420, 121430,
            LocalDateTime.of(2022, Month.OCTOBER, 4, 12, 45, 0),
            LocalDateTime.of(2022, Month.OCTOBER, 3, 12, 0, 0),
        )} returns false

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/submission-reminders/create")
            .bodyValue(
                mapOf(
                    "submission" to 121430,
                    "name" to "DÚ 2",
                    "course" to "Účetnictví I. (1FU201)",
                    "reminder" to "2022-10-03T12:00:00",
                    "due" to "2022-10-04T12:45:00",
                )
            )
            .exchange()
            .expectStatus()
            .isOk
            .expectBody()
            .jsonPath("reminders")
            .isArray

        coVerify { reminderRepository.save(any()) }

        assertTrue(slot.isCaptured)
        assertEquals(121430, slot.captured.submission)
        assertEquals("DÚ 2", slot.captured.name)
        assertEquals("Účetnictví I. (1FU201)", slot.captured.course)
        assertEquals(LocalDateTime.of(2022, Month.OCTOBER, 3, 12, 0, 0), slot.captured.reminder)
        assertEquals(LocalDateTime.of(2022, Month.OCTOBER, 4, 12, 45, 0), slot.captured.due)
    }

    private fun authenticate(account: Account): Authentication {
        return JwtAuthenticationToken(account, "valid.jwt.token")
    }
}