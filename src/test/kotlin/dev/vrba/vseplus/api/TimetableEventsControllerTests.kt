package dev.vrba.vseplus.api

import com.ninjasquad.springmockk.MockkBean
import dev.vrba.vseplus.api.domain.Account
import dev.vrba.vseplus.api.domain.TimetableEvent
import dev.vrba.vseplus.api.repository.AccountRepository
import dev.vrba.vseplus.api.repository.TimetableEventsRepository
import dev.vrba.vseplus.api.security.jwt.JwtAuthenticationToken
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.slot
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flowOf
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.core.Authentication
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient
import java.time.LocalDateTime
import java.time.Month

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureWebTestClient
class TimetableEventsControllerTests {

    @Autowired
    private lateinit var client: WebTestClient

    @MockkBean
    private lateinit var accountRepository: AccountRepository

    @MockkBean
    private lateinit var timetableEventsRepository: TimetableEventsRepository

    @Test
    fun `test that users cannot access timetable events without authentication`() {
        client.get()
            .uri("/api/v1/timetable-events")
            .exchange()
            .expectStatus()
            .isUnauthorized
    }

    @Test
    fun `test that users can access timetable events with authentication`() {
        val account = Account(id = 420, username = "vrbj04")
        val events = listOf(
            TimetableEvent(
                account = 420,
                datetime = LocalDateTime.of(2022, Month.OCTOBER, 10, 11, 0),
                course = "4SA310 IT Governance",
                type = "test",
                note = "Test z procesních diagramů or idk"
            )
        )

        coEvery { accountRepository.findByUsername("vrbj04") } returns account
        coEvery { timetableEventsRepository.findAllByAccount(account.id) } returns events.asFlow()

        val authentication = authenticate(account)

        client
            .mutateWith(mockAuthentication(authentication))
            .get()
            .uri("/api/v1/timetable-events")
            .exchange()
            .expectStatus()
            .isOk
    }

    @Test
    fun `test that users cannot create timetable events with invalid course`() {
        val account = Account(420, "vrbj04")
        val authentication = authenticate(account)

        coEvery { accountRepository.findByUsername("vrbj04") } returns account

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/create")
            .bodyValue(
                mapOf(
                    "type" to "test",
                    "note" to "Something",
                    "datetime" to "2022-10-10T11:00:00"
                )
            )
            .exchange()
            .expectStatus()
            .isBadRequest

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/create")
            .bodyValue(
                mapOf(
                    "course" to "",
                    "type" to "test",
                    "note" to "Something",
                    "datetime" to "2022-10-10T11:00:00"
                )
            )
            .exchange()
            .expectStatus()
            .isBadRequest

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/create")
            .bodyValue(
                mapOf(
                    "course" to listOf("4SA310 IT Governance"),
                    "type" to "test",
                    "note" to "Something",
                    "datetime" to "2022-10-10T11:00:00"
                )
            )
            .exchange()
            .expectStatus()
            .isBadRequest
    }

    @Test
    fun `test that users cannot create timetable events with invalid type`() {
        val account = Account(420, "vrbj04")
        val authentication = authenticate(account)

        coEvery { accountRepository.findByUsername("vrbj04") } returns account

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/create")
            .bodyValue(
                mapOf(
                    "course" to "4SA310 IT Governance",
                    "note" to "Something",
                    "datetime" to "2022-10-10T11:00:00"
                )
            )
            .exchange()
            .expectStatus()
            .isBadRequest

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/create")
            .bodyValue(
                mapOf(
                    "course" to "4SA310 IT Governance",
                    "type" to listOf("test"),
                    "note" to "Something",
                    "datetime" to "2022-10-10T11:00:00"
                )
            )
            .exchange()
            .expectStatus()
            .isBadRequest
    }

    @Test
    fun `test that users cannot create timetable events with invalid note`() {
        val account = Account(420, "vrbj04")
        val authentication = authenticate(account)

        coEvery { accountRepository.findByUsername("vrbj04") } returns account

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/create")
            .bodyValue(
                mapOf(
                    "course" to "4SA310 IT Governance",
                    "type" to "test",
                    "datetime" to "2022-10-10T11:00:00"
                )
            )
            .exchange()
            .expectStatus()
            .isBadRequest

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/create")
            .bodyValue(
                mapOf(
                    "course" to "4SA310 IT Governance",
                    "type" to "test",
                    "note" to listOf("Something"),
                    "datetime" to "2022-10-10T11:00:00"
                )
            )
            .exchange()
            .expectStatus()
            .isBadRequest
    }

    @Test
    fun `test that users can create timetable events with valid parameters`() {
        val account = Account(420, "vrbj04")
        val authentication = authenticate(account)
        val events = listOf(
            TimetableEvent(
                1,
                420,
                LocalDateTime.of(2022, Month.OCTOBER, 10, 11, 0, 0),
                "4SA310 IT Governance",
                "test",
                "Some note idk"
            )
        )

        val slot = slot<TimetableEvent>()

        coEvery { accountRepository.findByUsername("vrbj04") } returns account
        coEvery { timetableEventsRepository.save(capture(slot)) } returnsArgument 0
        coEvery { timetableEventsRepository.findAllByAccount(420) } answers { (events + slot.captured).asFlow() }

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/create")
            .bodyValue(
                mapOf(
                    "course" to "4SA311 IT Governance 2 gganbustare",
                    "type" to "test",
                    "note" to "Something else",
                    "datetime" to "2022-10-17T11:00:00"
                )
            )
            .exchange()
            .expectStatus()
            .isOk
            .expectBody()
            .jsonPath("$.events")
            .isArray
            .jsonPath("$.events[0].course").isEqualTo("4SA310 IT Governance")
            .jsonPath("$.events[0].type").isEqualTo("test")
            .jsonPath("$.events[0].datetime").isEqualTo("2022-10-10T11:00")
            .jsonPath("$.events[0].note").isEqualTo("Some note idk")
            .jsonPath("$.events[1].course").isEqualTo("4SA311 IT Governance 2 gganbustare")
            .jsonPath("$.events[1].type").isEqualTo("test")
            .jsonPath("$.events[1].datetime").isEqualTo("2022-10-17T11:00")
            .jsonPath("$.events[1].note").isEqualTo("Something else")

        coVerify { timetableEventsRepository.save(slot.captured) }

        assertTrue(slot.isCaptured)
        assertEquals("4SA311 IT Governance 2 gganbustare", slot.captured.course)
        assertEquals("test", slot.captured.type)
        assertEquals(LocalDateTime.of(2022, 10, 17, 11, 0), slot.captured.datetime)
        assertEquals("Something else", slot.captured.note)
    }

    @Test
    fun `test that users cannot update timetable events that doesn't exist`() {
        val account = Account(420, "vrbj04")
        val authentication = authenticate(account)

        coEvery { accountRepository.findByUsername("vrbj04") } returns account
        coEvery { timetableEventsRepository.findByIdAndAccount(2, 420) } returns null

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/2/update")
            .bodyValue(
                mapOf(
                    "type" to "other",
                    "note" to "Something else, but updated",
                )
            )
            .exchange()
            .expectStatus()
            .isNotFound
    }

    @Test
    fun `test that users cannot delete timetable events that doesn't exist`() {
        val account = Account(420, "vrbj04")
        val authentication = authenticate(account)

        coEvery { accountRepository.findByUsername("vrbj04") } returns account
        coEvery { timetableEventsRepository.findByIdAndAccount(1, 420) } returns null
        coEvery { timetableEventsRepository.findAllByAccount(420) } answers { emptyFlow() }

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/1/delete")
            .exchange()
            .expectStatus()
            .isNotFound
    }

    @Test
    fun `test that users can delete timetable events that exist`() {
        val account = Account(420, "vrbj04")
        val authentication = authenticate(account)
        val event = TimetableEvent(
            1,
            420,
            LocalDateTime.of(2022, Month.OCTOBER, 17, 11, 0, 0),
            "4SA310 IT Governance",
            "test",
            "Some note idk"
        )

        coEvery { accountRepository.findByUsername("vrbj04") } returns account
        coEvery { timetableEventsRepository.delete(event) } returns Unit
        coEvery { timetableEventsRepository.findByIdAndAccount(1, 420) } returns event
        coEvery { timetableEventsRepository.findAllByAccount(420) } answers { emptyFlow() }

        client
            .mutateWith(mockAuthentication(authentication))
            .post()
            .uri("/api/v1/timetable-events/1/delete")
            .exchange()
            .expectStatus()
            .isOk
            .expectBody()
            .jsonPath("$.events")
            .isArray
            .jsonPath("$.events")
            .isEmpty

        coVerify { timetableEventsRepository.delete(event) }
    }

    private fun authenticate(account: Account): Authentication {
        return JwtAuthenticationToken(account, "valid.jwt.token")
    }
}