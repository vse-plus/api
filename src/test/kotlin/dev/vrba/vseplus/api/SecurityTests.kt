package dev.vrba.vseplus.api

import com.ninjasquad.springmockk.MockkBean
import dev.vrba.vseplus.api.configuration.JwtConfiguration
import dev.vrba.vseplus.api.domain.Account
import dev.vrba.vseplus.api.repository.AccountRepository
import dev.vrba.vseplus.api.security.jwt.JwtBearerToken
import dev.vrba.vseplus.api.security.jwt.JwtSupport
import io.mockk.coEvery
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.env.Environment
import org.springframework.http.HttpHeaders
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient
import java.time.Clock
import java.time.Duration
import java.time.Instant

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureWebTestClient
class SecurityTests {

    @Autowired
    private lateinit var client: WebTestClient

    @Autowired
    private lateinit var configuration: JwtConfiguration

    @Autowired
    private lateinit var environment: Environment

    @MockkBean
    private lateinit var support: JwtSupport

    @MockkBean
    private lateinit var repository: AccountRepository

    @MockkBean
    private lateinit var clock: Clock

    @Test
    fun `user can access non-api endpoints without authentication`() {
        client.post()
            .uri("/some/path/that/doesnt/start/with/api/v1")
            .exchange()
            .expectStatus()
            .isNotFound
    }

    @Test
    fun `user can access authentication api endpoints without authentication`() {
        client.post()
            .uri("/api/v1/account/endpoint")
            .exchange()
            .expectStatus()
            .isNotFound
    }

    @Test
    fun `user cannot access nested authentication api endpoints without authentication`() {
        client.post()
            .uri("/api/v1/account/nested/endpoint")
            .exchange()
            .expectStatus()
            .isUnauthorized
    }

    @Test
    fun `user cannot access api endpoints without authentication`() {
        client.get()
            .uri("/api/v1/some/path/that/is/not/account")
            .exchange()
            .expectStatus()
            .isUnauthorized
    }

    @Test
    fun `user can access api endpoints with valid JWT token`() {
        val token = "valid.jwt.token"
        val account = Account(id = 420, username = "vrbj04")

        every { support.retrieveUsername(JwtBearerToken(token)) } returns account.username
        every { support.validateToken(JwtBearerToken(token), account) } returns true
        coEvery { repository.findByUsername(account.username) } returns account

        client.post()
            .uri("/api/v1/protected/endpoint")
            .header(HttpHeaders.AUTHORIZATION, "Bearer $token")
            .exchange()
            .expectStatus()
            .isNotFound
    }

    @Test
    fun `user cannot access api endpoints with invalid JWT token`() {
        val token = "invalid.jwt.token"
        val account = Account(id = 420, username = "vrbj04")

        every { support.retrieveUsername(JwtBearerToken(token)) } returns "vrbj04"
        every { support.validateToken(JwtBearerToken(token), account) } returns false
        coEvery { repository.findByUsername(account.username) } returns account

        client.post()
            .uri("/api/v1/protected/endpoint")
            .header(HttpHeaders.AUTHORIZATION, "Bearer $token")
            .exchange()
            .expectStatus()
            .isUnauthorized
    }

    @Test
    fun `user cannot access api endpoint with expired JWT token`() {
        val base = Instant.now()
        val expired = base + Duration.ofDays(250)

        every { clock.instant() } returns base andThen expired

        val token = JwtSupport(clock, configuration, environment).generateToken("vrbj04")
        val account = Account(id = 420, username = "vrbj04")

        every { support.validateToken(token, account) } answers { callOriginal() }
        coEvery { repository.findByUsername(account.username) } returns account

        client.post()
            .uri("/api/v1/protected/endpoint")
            .header(HttpHeaders.AUTHORIZATION, "Bearer $token")
            .exchange()
            .expectStatus()
            .isUnauthorized
    }
}